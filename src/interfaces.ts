import * as firebase from 'firebase/app';

export interface BallMeasurement {
  name: string;
  type: string;
  a: number;
  b: number;
  c: number;
  d: number;
  distances: Array<number>;
  scattering?: number;
  scatteringArea?: number;
}

export interface MeasurementDocument {
  name: string;
  note?: string;
  created: firebase.firestore.FieldValue;
  category: string;
  id?: string;
  balls?: Array<BallMeasurement>;
}

export interface Category {
  name: string;
  note: string;
  created: firebase.firestore.FieldValue;
  id?: string;
  measurements?: Array<MeasurementDocument>;
}
