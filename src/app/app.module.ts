import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule, MatDividerModule, MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatListModule,
  MatMenuModule, MatSelectModule, MatStepperModule, MatTableModule, MatTabsModule, MatToolbarModule, MatTooltipModule
} from '@angular/material';
import { LoginPageComponent } from './login-page/login-page.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { BocciaMenuComponent } from './boccia-menu/boccia-menu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { NewCategoryDialogComponent } from './new-category-dialog/new-category-dialog.component';
import { BallDetailFormComponent } from './ball-detail-form.component/ball-detail-form.component';
import { MeasurementDetailComponent } from './measurement-detail/measurement-detail.component';
import { NewBallDialogComponent } from './new-ball-dialog/new-ball-dialog.component';
import {NewMeasurementDialogComponent} from './new-measurement-dialog/new-measurement-dialog.component';
import { PlaygroundComponent } from './playground/playground.component';
import { RealImpactPlaygroundComponent } from './real-impact-playground/real-impact-playground.component';
import { AccuracyPlaygroundComponent } from './accuracy-playground/accuracy-playground.component';
import { PlayersDetailsComponent } from './players-details/players-details.component';
import { OrderDataTableComponent } from './order-data-table/order-data-table.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    BocciaMenuComponent,
    DashboardComponent,
    NewCategoryDialogComponent,
    NewMeasurementDialogComponent,
    BallDetailFormComponent,
    MeasurementDetailComponent,
    NewBallDialogComponent,
    PlaygroundComponent,
    RealImpactPlaygroundComponent,
    AccuracyPlaygroundComponent,
    PlayersDetailsComponent,
    OrderDataTableComponent,
  ],
  entryComponents: [
    NewBallDialogComponent,
    NewMeasurementDialogComponent
  ],
  imports: [

    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    // Material
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatMenuModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatDividerModule,
    MatListModule,
    FormsModule,
    MatTooltipModule,
    MatTableModule,
    MatStepperModule,
    MatToolbarModule,
    MatExpansionModule,
    MatSelectModule,
    // Angularfire
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private translateService: TranslateService) {
    translateService.setDefaultLang('en');

  }
}
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
