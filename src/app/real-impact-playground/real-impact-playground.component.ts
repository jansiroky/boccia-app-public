import {AfterViewInit, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {BallMeasurement} from '../../interfaces';
import {fabric} from 'fabric';
import {CalculationService} from '../calculation.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-real-impact-playground',
  templateUrl: './real-impact-playground.component.html',
  styleUrls: ['./real-impact-playground.component.scss']
})
export class RealImpactPlaygroundComponent implements AfterViewInit, OnChanges {
  @Input() balls;

  public canvas: any;
  public canvasWidth: number;
  public canvasHeight: number;
  public scale: number = 0.3;


  constructor(
    private calculationService: CalculationService,
    private translateService: TranslateService
  ) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.balls.currentValue) {
      changes.balls.currentValue.forEach(ball => {
        this.renderBall(ball);
      });

      this.canvas.forEachObject((element) => {
        element.selectable = false;
        element.hoverCursor = 'default';
      });
    }


  }

  ngAfterViewInit() {
    this.canvasWidth = document.getElementById('canvas-wrapper').offsetWidth;
    this.canvasHeight = this.canvasWidth * (12.5 / 6);

    this.canvas = new fabric.Canvas('a', {interactive: false, selection: false});

    this.canvas.setDimensions({
      width: this.canvasWidth,
      height: this.canvasHeight
    });
    this.translateService.get('arrangement').subscribe(translatedString => {
      const headLine = new fabric.Text(translatedString, {
        originX: 'center',
        originY: 'center',
        top: 20,
        left: this.canvasWidth / 2,
        fontFamily: 'Arial',
        fontWeight: 300,
        fontSize: 14,
        fill: 'black'
      });
      this.canvas.add(headLine);
    });

    const bottomText = new fabric.Text('.', {
      originX: 'center',
      originY: 'center',
      top: this.canvasHeight - 20,
      left: this.canvasWidth / 2,
      fontFamily: 'Arial',
      fontWeight: 300,
      fontSize: 9,
      fill: 'black'
    });

    for (let i = 0; i < 6; i++) {
      const rectangle = new fabric.Rect({
        top: this.canvasHeight + 1,
        left: i * this.canvasWidth / 6 - 1,
        width: this.canvasWidth / 6,
        height: this.canvasHeight / 12.5 * 2.5,
        stroke: 'black',
        fill: 'white',
        originY: 'bottom',
        strokeWidth: 2,
      });
      this.canvas.add(rectangle);

    }

    const verticalLine = new fabric.Line([
      this.canvasWidth / 2 - 0.5,
      this.canvasHeight * 0.03,
      this.canvasWidth / 2 - 0.5,
      this.canvasHeight
    ], {fill: 'black', stroke: 'black', strokeWidth: 1});

    const horizontalLine = new fabric.Line([
      this.canvasWidth,
      this.canvasHeight

    ], {fill: 'black', stroke: 'black', strokeWidth: 1});


    const redLine = new fabric.Line([
      this.canvasWidth / 4 - 0.5,
      0,
      this.canvasWidth / 4 - 0.5,
      this.canvasHeight
    ], {fill: 'red', stroke: 'red', strokeWidth: 1});

    const blueLine = new fabric.Line([
      this.canvasWidth / 4 * 3 - 0.5,
      0,
      this.canvasWidth / 4 * 3 - 0.5,
      this.canvasHeight
    ], {fill: 'RGBA(130, 177, 255, .3)', stroke: 'RGBA(130, 177, 255, .3)', strokeWidth: 1});


    const verticalCross = new fabric.Line([
      this.canvasWidth / 2,
      this.canvasHeight / 12.5 * 5 - 10,
      this.canvasWidth / 2,
      this.canvasHeight / 12.5 * 5 + 10
    ], {
      originX: 'center',
      originY: 'center',
      fill: 'black',
      stroke: 'black',
      strokeWidth: 2
    });

    const horizontalCross = new fabric.Line([
      this.canvasWidth / 2 - 10,
      this.canvasHeight / 12.5 * 5,
      this.canvasWidth / 2 + 10,
      this.canvasHeight / 12.5 * 5
    ], {
      originX: 'center',
      originY: 'center',
      fill: 'black',
      stroke: 'black',
      strokeWidth: 2
    });

    const leftVline = new fabric.Line([
      0,
      this.canvasHeight / 12.5 * 7,
      this.canvasWidth / 2,
      this.canvasHeight / 12.5 * 8.5,
    ], {
      originX: 'center',
      originY: 'center',
      fill: 'black',
      stroke: 'black',
      strokeWidth: 2
    });

    const rightVline = new fabric.Line([
      this.canvasWidth,
      this.canvasHeight / 12.5 * 7,
      this.canvasWidth / 2,
      this.canvasHeight / 12.5 * 8.5,
    ], {
      originX: 'center',
      originY: 'center',
      fill: 'black',
      stroke: 'black',
      strokeWidth: 2
    });


    setTimeout(() => {
      this.canvas.add(bottomText, verticalLine, blueLine, redLine, horizontalLine, verticalCross, horizontalCross,
        leftVline, rightVline);
    }, 100);
  }

  calculateTop(ball: BallMeasurement) {
    const availableHeight = this.canvasHeight / 12.5 * 10;
    const cmToPx = availableHeight / 1000;
    return (1000 - ball.d) * cmToPx;
  }

  renderBall(ball: BallMeasurement) {
    const radius: number = ball.scatteringArea * this.scale; // => POLOMER
    const averageDistance: number = this.calculateTop(ball);

    // adding Circle to canvas
    const leftPadding: number = ((this.canvasWidth / 4) * (ball.type === 'red' ? 1 : ball.type === 'blue' ? 3 : 2));
    const ellipseXY = this.calculationService.calculateEllipseXY(ball.a, ball.b, radius);

    const ballEllipse = new fabric.Ellipse({
      rx: ellipseXY.x,
      ry: ellipseXY.y,
      originX: 'center',
      top: averageDistance,
      left: leftPadding + ball.c,
      fill: ball.type === 'jack' ? 'white' : (ball.type === 'red' ? 'RGBA(255, 0, 0, .3)' : 'RGBA(130, 177, 255, .3)'),
      strokeWidth: 0.5,
      stroke: 'black'
    });

    const ballScatteringAreaText = new fabric.Text(ball.scatteringArea.toFixed(0) + ' cm', {
      originX: 'center',
      originY: 'center',
      top: averageDistance + ellipseXY.y,
      left: leftPadding + (ball.type === 'blue' ? -45 : (ball.type === 'red' ? 45 : 45)) + ball.c,
      fontFamily: 'Arial',
      fontWeight: 300,
      fontSize: 10,
      fill: 'black'
    });

    const ballNameText = new fabric.Text(ball.name ? ball.name + ' -- ' + ball.d : '', {
      originX: 'center',
      originY: 'center',
      top: averageDistance + ellipseXY.y,
      left: leftPadding + (ball.type === 'blue' ? 55 : (ball.type === 'red' ? -55 : -55)) + ball.c,
      fontFamily: 'Arial',
      fontWeight: 300,
      fontSize: 10,
      fill: 'black'
    });

    this.canvas.add(ballEllipse, ballScatteringAreaText, ballNameText);
  }
}
