import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public loginForm: FormGroup = this.formBuilder.group({
    email: [null, [Validators.required, Validators.email]]
  });
  public emailWasSent: boolean = false;
  public emailAddress: string;

  constructor(
    public authService: AuthService,
    public formBuilder: FormBuilder,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.emailAddress = localStorage.getItem('emailAddress');
    if (this.emailAddress != null) {
      this.emailWasSent = true;
    }
    this.authService.afAuth.user.subscribe(user => {
      if (user != null) {
        this.router.navigate(['merania']);

      }
    });
  }

  loginWithEmail() {
    if (this.loginForm.valid) {
      const email = this.loginForm.value.email;
      this.authService.loginWithEmailLink(email);
      this.emailWasSent = true;
      localStorage.setItem('emailAddress', email);
      this.emailAddress = email;
    }
  }

  public cancelEmailAlert(): void {
    this.emailWasSent = false;
    localStorage.removeItem('emailAddress');
  }
}
