import {Component, OnInit} from '@angular/core';
import countries from './countries';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../data.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-players-details',
  templateUrl: './players-details.component.html',
  styleUrls: ['./players-details.component.scss']
})
export class PlayersDetailsComponent implements OnInit {
  public countries: Array<{ name: string, code: string }> = countries;
  public years: Array<number> = [];
  level = ['amateur', 'professional'];
  playerDetailForm: FormGroup = this.formBuilder.group({
    language: ['en', [Validators.required]],
    name: [null, [Validators.required, Validators.minLength(3),
      Validators.maxLength(40)]],
    brand: [null, [Validators.required, Validators.minLength(2),
      Validators.maxLength(40)]],
    country: [null, [Validators.required]],
    level: [null, [Validators.required]],
    started: [null, [Validators.required]]
  });

  constructor(
    private translateService: TranslateService,
    public formBuilder: FormBuilder,
    private dataService: DataService,
    private afAuth: AngularFireAuth,
  ) {

  }

  useLanguage(languageCode: string) {
    this.translateService.use(languageCode);
  }

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    for (let year = currentYear; year >= currentYear - 20; year--) {
      this.years.push(year);
    }

    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.dataService.getUserInfo().subscribe(userInfo => {
          this.playerDetailForm.patchValue(userInfo);
        });
      }
    });
  }

  submit() {
    this.dataService.markAllAsTouched(this.playerDetailForm);
    if (this.playerDetailForm.valid) {
      this.dataService.updateInfo(this.playerDetailForm.getRawValue());
    }
  }
}
