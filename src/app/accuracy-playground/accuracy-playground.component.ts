import {AfterViewInit, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {BallMeasurement} from '../../interfaces';
import {fabric} from 'fabric';

@Component({
  selector: 'app-accuracy-playground',
  templateUrl: './accuracy-playground.component.html',
  styleUrls: ['./accuracy-playground.component.scss']
})
  export class AccuracyPlaygroundComponent implements AfterViewInit, OnChanges {
    @Input() balls;

    public canvas: any;
    public canvasWidth: number;
    public canvasHeight: number;
    public scale: number = 0.3;


    constructor() {
    }

    ngOnChanges(changes: SimpleChanges) {
      if (changes.balls.currentValue) {
      changes.balls.currentValue.forEach(ball => {
        this.renderBall(ball);
      });
      this.canvas.forEachObject((element) => {
        element.selectable = false;
        element.hoverCursor = 'default' ;
      });
    }


  }

  ngAfterViewInit() {
    this.canvasWidth = document.getElementById('canvas-wrapper').offsetWidth;
    this.canvasHeight = this.canvasWidth * (10 / 6);

    this.canvas = new fabric.Canvas('c', {interactive: false, selection: false});

    this.canvas.setDimensions({
      width: this.canvasWidth,
      height: this.canvasHeight
    });
  }

  renderBall(ball: BallMeasurement) {
    // filters all balls of the same type, sorts them from the most accurate
    const sameBallsArray = this.balls
      .filter(_ball => _ball.type === ball.type)
      .sort((a, b) => a.scatteringArea - b.scatteringArea);
    // Accuracy order of this ball in its group
    const ballOrder = sameBallsArray.indexOf(ball);
    // Finding out the ball with best scattering area and adding it to playground
    const betterBallsTotalRadius = sameBallsArray.slice(0, ballOrder)
      .reduce((a, b) => a + b.scatteringArea * this.scale + .5, 0); // .5 adds vertical space between balls
    const radius: number = ball.scatteringArea * this.scale; // => POLOMER
    // adding Circle to canvas

    const leftPadding: number = ((this.canvasWidth / 4) * (ball.type === 'red' ? 1 : ball.type === 'blue' ? 3 : 2));

    const ballCircle = new fabric.Circle({
      radius: radius,
      originX: 'center',
      top: 50 + betterBallsTotalRadius * 2,
      left: leftPadding,
      fill: ball.type === 'jack' ? 'white' : (ball.type === 'red' ? 'RGBA(255, 0, 0, .3)' : 'RGBA(130, 177, 255, .3)'),
      strokeWidth: 0.5,
      stroke: 'black'
    });

    const ballScatteringAreaText = new fabric.Text(ball.scatteringArea.toFixed(0), {
      originX: 'center',
      originY: 'center',
      top: 50 + betterBallsTotalRadius * 2 + radius,
      left: leftPadding,
      fontFamily: 'Arial',
      fontWeight: 300,
      fontSize: 10,
      fill: 'black'
    });

    const ballNameText = new fabric.Text(ball.name ? ball.name + ' -- ' + ball.d : '', {
      originX: 'center',
      originY: 'center',
      top: 50 + betterBallsTotalRadius * 2 + radius,
      left: leftPadding + (ball.type === 'blue' ? 70 : (ball.type === 'red' ? -70 : -50)),
      fontFamily: 'Arial',
      fontWeight: 300,
      fontSize: 10,
      fill: 'black'
    });

    this.canvas.add(ballCircle, ballScatteringAreaText, ballNameText);
  }
}
