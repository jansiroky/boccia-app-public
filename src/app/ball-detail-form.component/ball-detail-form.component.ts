import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BallMeasurement} from '../../interfaces';
import {DataService} from '../data.service';

@Component({
  selector: 'app-ball-detail-form',
  templateUrl: './ball-detail-form.component.html',
  styleUrls: ['./ball-detail-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BallDetailFormComponent implements OnChanges {

  @Output() ballMeasurementData: EventEmitter<BallMeasurement> = new EventEmitter();
  @Output() isValid: EventEmitter<boolean> = new EventEmitter();
  @Input() ballMeasurementDataInput: BallMeasurement;

  public selectedColor: string;
  public ballTypes: Array<string> = ['red', 'blue', 'jack'];

  public ballForm: FormGroup;

  public abcd: Array<{ label: string; formControlName: string; description: string; error: string; }> = [{
    label: 'A',
    formControlName: 'a',
    description: 'horizontal_scattering',
    error: 'validator_1_300'

  }, {
    label: '️B',
    formControlName: 'b',
    description: 'vertical_scattering',
    error:  'validator_1_300'
  }, {
    label: 'C',
    formControlName: 'c',
    description: 'average_deflection',
    error:  'validator_-100_100'
  }, {
    label: 'D',
    formControlName: 'd',
    description: 'average_distance',
    error:  'validator_50_1000'
  }];

  constructor(
    public formBuilder: FormBuilder,
    public dataService: DataService
  ) {
    this.ballForm = this.formBuilder.group({
      type: [null, [Validators.required]],
      name: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(30)]],
      a: [null, [Validators.required, Validators.min(1), Validators.max(300)]],
      b: [null, [Validators.required, Validators.min(1), Validators.max(300)]],
      c: [null, [Validators.required, Validators.min(-100), Validators.max(100)]],
      d: [null, [Validators.required, Validators.min(50), Validators.max(1000)]],
      distances: this.formBuilder.array([
        [null, [Validators.required, Validators.min(1), Validators.max(1000)]],
        [null, [Validators.required, Validators.min(1), Validators.max(1000)]],
        [null, [Validators.required, Validators.min(1), Validators.max(1000)]]
      ])
    });

    this.ballForm.statusChanges.subscribe(change => {
      this.isValid.emit(this.ballForm.valid);
      if (this.ballForm.valid) {
        this.ballMeasurementData.emit(this.ballForm.getRawValue());
      }
    });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.ballForm.patchValue(changes.ballMeasurementDataInput.currentValue);
    this.selectColor(changes.ballMeasurementDataInput.currentValue.type);
  }

  selectColor(color) {
    this.selectedColor = color;
    this.ballForm.patchValue({
      type: color
    });
  }

  addDistanceInput(): void {
    const distancesFormArray = this.ballForm.get('distances') as FormArray;
    distancesFormArray.push(this.formBuilder.control(null, [Validators.required, Validators.min(1), Validators.max(1000)]));
  }

  validate(): void {
    this.dataService.markAllAsTouched(this.ballForm);
  }
}
