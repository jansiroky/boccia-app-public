import {Component, Input, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {ActivatedRoute} from '@angular/router';
import {CalculationService} from '../calculation.service';
import {BallMeasurement, MeasurementDocument} from '../../interfaces';
import {AngularFireAuth} from '@angular/fire/auth';
import {OrderDataTableData} from '../order-data-table/order-data-table.component';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {

  public measurementId: string;
  public balls: Array<BallMeasurement>;
  public distances: Array<OrderDataTableData>;
  public scattering: Array<OrderDataTableData>;
  public deflection: Array<OrderDataTableData>;
  public measurement: MeasurementDocument;


  constructor(
    private afAuth: AngularFireAuth,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private calculationsService: CalculationService
  ) {
    this.measurementId = this.activatedRoute.snapshot.params.measurementId;
  }

  ngOnInit() {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.dataService.getMeasurement(this.measurementId).subscribe(measurementDocument => {
          this.measurement = measurementDocument;
          if (measurementDocument == null) {
            return;
          }

          if (measurementDocument.balls != null) {
            measurementDocument.balls.forEach((ball) => {
              ball.scattering = this.calculationsService.scattering(ball.distances);
              ball.scatteringArea = this.calculationsService.scattering(ball.distances);
            });
          }
          this.balls = measurementDocument.balls;
          this.distances = this.balls.map(ball => {
              return {ballName: ball.name, value: ball.d, type: ball.type};
            }
          );
          this.scattering = this.balls.map(ball => {
              return {ballName: ball.name, value: ball.a, type: ball.type};
            }
          );
          this.deflection = this.balls.map(ball => {
              return {ballName: ball.name, value: ball.c, type: ball.type};
            }
          );
        });
      }
    });
  }
}
