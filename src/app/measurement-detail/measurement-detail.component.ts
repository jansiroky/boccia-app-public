import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatDialog} from '@angular/material';
import {NewBallDialogComponent} from '../new-ball-dialog/new-ball-dialog.component';
import {DataService} from '../data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {BallMeasurement, MeasurementDocument} from '../../interfaces';
import {CalculationService} from '../calculation.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {BallDetailFormComponent} from '../ball-detail-form.component/ball-detail-form.component';

@Component({
  selector: 'app-measurement-detail',
  templateUrl: './measurement-detail.component.html',
  styleUrls: ['./measurement-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MeasurementDetailComponent implements OnInit {

  @ViewChild('ballDetailForm') ballDetailForm: BallDetailFormComponent;

  public measurementId: string;
  public measurementDocument: MeasurementDocument;
  public unsavedBallChange: { index: number, data: BallMeasurement };
  public expandedBallMeasurementIndex: number | null = null;
  public ballMeasurementDataValid: boolean = true;

  constructor(
    private dataService: DataService,
    private afAuth: AngularFireAuth,
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog,
    private calculationsService: CalculationService,
    private router: Router,
  ) {
    this.measurementId = this.activatedRoute.snapshot.params.measurementId;
  }

  ngOnInit() {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.dataService.getMeasurement(this.measurementId).subscribe(measurementDocument => {
          if (measurementDocument.balls != null) {
            measurementDocument.balls.forEach((ball) => {
              ball.scattering = this.calculationsService.scattering(ball.distances);
              ball.scatteringArea = this.calculationsService.scatteringArea(ball.a, ball.b);
            });
          }
          this.measurementDocument = measurementDocument;
        });
      }
    });

  }

  openBallDialog() {
    this.matDialog.open(NewBallDialogComponent, {
      maxHeight: '90vh',
      autoFocus: false,
      disableClose: true,
      data: {
        measurementId: this.measurementId
      }
    });
  }

  formatDistances(distances: Array<number>) {
    let result = '';
    distances.forEach((value, index) => {
      result += ' <span class="value">' + value + '</span>' + (index === distances.length - 1 ? '' : ',');
    });
    return result;
  }

  saveChanges() {
    this.ballDetailForm.validate();
    if (this.unsavedBallChange != null && this.ballMeasurementDataValid) {
      this.dataService.updateBallMeasurement(this.measurementId, this.measurementDocument.balls,
        this.unsavedBallChange.index, this.unsavedBallChange.data);
    }
  }

  deleteMeasurement() {
    if (confirm('Naozaj si prajete VYMAZAŤ meranie ' + this.measurementDocument.name + '?\nTáto operácia je nevratná.')) {
      this.dataService.deleteMeasurement(this.measurementId);
      this.router.navigate(['merania']);
    }
  }

  deleteBall (ball) {
    if (confirm('Naozaj si prajete VYMAZAŤ loptu ' + ball.name + '?\nTáto operácia je nevratná.')) {
      this.dataService.deleteBall(this.measurementId, ball);
    }
  }
}




