import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {Category, MeasurementDocument} from '../../interfaces';
import {MatDialog} from '@angular/material';
import {NewCategoryDialogComponent} from '../new-category-dialog/new-category-dialog.component';
import {NewMeasurementDialogComponent} from '../new-measurement-dialog/new-measurement-dialog.component';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public categories: Array<Category>;
  private uid: string;

  constructor(
    private afAuth: AngularFireAuth,
    private data: DataService,
    private matDialog: MatDialog
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.data.getCategories().subscribe(categories => {
          this.data.getMeasurements().subscribe(measurements => {
            categories.forEach((category, index) => {
              const categoryId = category.id;
              categories[index].measurements = measurements.filter(measurement => {
                return measurement.category === categoryId;
              });
            });
            this.categories = categories;
          });
        });
      } else {
        // Empty the value when user signs out
        this.uid = null;
      }
    });

  }

  ngOnInit() {

  }

  openNewCategoryDialog() {
    this.matDialog.open(NewCategoryDialogComponent, {
      minWidth: 300,
      width: '30%',
    });
  }

  deleteCategory(category: Category) {
    if (confirm('Naozaj si prajete VYMAZAŤ ' + category.name + '?\nTáto operácia je nevratná.')) {
      this.data.deleteCategory(category.id);
    }
  }

  openNewMeasurementDialog(id: string) {
    this.matDialog.open(NewMeasurementDialogComponent, {
      minWidth: 300,
      width: '30%',
      data: {
        id: id
      }
    });
  }
}
