import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../data.service';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-new-category-dialog',
  templateUrl: './new-category-dialog.component.html',
  styleUrls: ['./new-category-dialog.component.scss']
})
export class NewCategoryDialogComponent {
  public newCategoryForm: FormGroup = this.formBuilder.group({
    name: [null, [Validators.required, Validators.minLength(3),
      Validators.maxLength(40)]],
    note: [null, Validators.maxLength(500)],
  });

  constructor(
    public formBuilder: FormBuilder,
    private data: DataService,
    private matDialog: MatDialogRef<any>,
  ) {
  }

  createNewCategory() {
    if (this.newCategoryForm.valid) {
      this.data.createCategory(this.newCategoryForm.getRawValue());
      this.matDialog.close();
    }
  }
}
