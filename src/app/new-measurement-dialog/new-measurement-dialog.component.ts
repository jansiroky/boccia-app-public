import {Component, Inject} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DataService} from '../data.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MeasurementDocument} from '../../interfaces';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-new-category-dialog',
  templateUrl: './new-measurement-dialog.component.html',
  styleUrls: ['./new-measurement-dialog.component.scss']
})
export class NewMeasurementDialogComponent {
  public newMeasurementForm: FormGroup = this.formBuilder.group({
    name: [null, [Validators.required, Validators.minLength(3),
      Validators.maxLength(40)]],
    note: [null, Validators.maxLength(500)]
  });

  constructor(
    public formBuilder: FormBuilder,
    private dataService: DataService,
    private matDialog: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) {
  }

  createNewMeasurement() {
    if (this.newMeasurementForm.valid) {
      const formData = this.newMeasurementForm.getRawValue();
      const data: MeasurementDocument = {
        name: formData.name,
        note: formData.note,
        created: firebase.firestore.FieldValue.serverTimestamp(),
        category: this.dialogData.id
      };
      this.dataService.createMeasurement(data);
      this.matDialog.close();
    }
  }
}
