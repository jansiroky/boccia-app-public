import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {CalculationService} from '../calculation.service';

@Component({
  selector: 'app-order-data-table',
  templateUrl: './order-data-table.component.html',
  styleUrls: ['./order-data-table.component.scss']
})
export class OrderDataTableComponent implements OnChanges {
  @Input() data: Array<OrderDataTableData>;
  @Input() title: string;
  public averageRed: number;
  public averageBlue: number;

  constructor(
    private calculationsService: CalculationService
  ) {
  }

  ngOnChanges() {
    if (this.data) {
      this.averageRed = this.calculationsService.average(this.data.filter(ball => ball.type === 'red').map(ball => ball.value));

      this.averageBlue = this.calculationsService.average(this.data.filter(ball => ball.type === 'blue').map(ball => ball.value));
    }
  }

}

export interface OrderDataTableData {
  ballName: string;
  value: number;
  type: string;
}
