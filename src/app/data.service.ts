import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {BallMeasurement, Category, MeasurementDocument} from '../interfaces';
import {map} from 'rxjs/operators';
import * as firebase from 'firebase/app';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private db: AngularFirestore,
    private authService: AuthService
  ) {
  }

  public getUserInfo(): Observable<any> {
    return this.db.doc(`users/${this.authService.uid}`).valueChanges();
  }

  public updateInfo(data) {
    this.db.doc(`users/${this.authService.uid}`).set(data);
  }

  public getCategories(): Observable<Array<Category>> {
    return this.db.collection(`users/${this.authService.uid}/categories`).snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data() as Category;
          const id = a.payload.doc.id;
          return {id, ...data};
        })));
  }

  public createCategory(data: Category) {
    this.db.collection(`users/${this.authService.uid}/categories`).add(data);
  }

  public deleteCategory(id: string) {
    this.db.doc(`users/${this.authService.uid}/categories/${id}`).delete();
  }

  public getMeasurements(): Observable<Array<MeasurementDocument>> {
    return this.db.collection(`users/${this.authService.uid}/measurements`).snapshotChanges()
      .pipe(
        map(actions => actions.map(a => {
          const data = a.payload.doc.data() as MeasurementDocument;
          const id = a.payload.doc.id;
          return {id, ...data};
        })));
  }

  public getMeasurement(id: string): Observable<MeasurementDocument> {
    return <Observable<MeasurementDocument>>this.db.doc(`users/${this.authService.uid}/measurements/${id}`).valueChanges();
  }

  public createMeasurement(data: MeasurementDocument) {
    this.db.collection(`users/${this.authService.uid}/measurements`).add(data);
  }

  public deleteMeasurement(id: string) {
    this.db.doc(`users/${this.authService.uid}/measurements/${id}`).delete();
  }

  public deleteBall(measurementId: string, ballMeasurement: BallMeasurement) {
    delete ballMeasurement.scattering;
    delete ballMeasurement.scatteringArea;
    this.db.doc(`users/${this.authService.uid}/measurements/${measurementId}`).update({
      balls: firebase.firestore.FieldValue.arrayRemove(ballMeasurement)
    });
  }

  public createBallMeasurement(measurementId: string, ballMeasurement: BallMeasurement): Promise<void> {
    return this.db.doc(`users/${this.authService.uid}/measurements/${measurementId}`).update({
      balls: firebase.firestore.FieldValue.arrayUnion(ballMeasurement)
    });
  }

  public updateBallMeasurement(measurementId: string, ballsArray: Array<BallMeasurement>, ballIndex: number, change: BallMeasurement): Promise<void> {
    const newBallArray = [...ballsArray];
    newBallArray[ballIndex] = change;

    return this.db.doc(`users/${this.authService.uid}/measurements/${measurementId}`).update({
      balls: newBallArray
    });
  }

  public markAllAsTouched(group: any) {
    group.markAsTouched({onlySelf: true});
    Object.keys(group.controls).forEach((key: string) => {
      if (group.controls[key].controls != null) {
        this.markAllAsTouched(group.controls[key]);
      } else {
        group.controls[key].markAsTouched();
      }
    });
  }
}
