import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataService} from '../data.service';
import {BallMeasurement} from '../../interfaces';
import {BallDetailFormComponent} from '../ball-detail-form.component/ball-detail-form.component';

@Component({
  selector: 'app-new-ball-dialog',
  templateUrl: './new-ball-dialog.component.html',
  styleUrls: ['./new-ball-dialog.component.scss']
})
export class NewBallDialogComponent {

  @ViewChild('ballDetailForm') ballDetailForm: BallDetailFormComponent;

  public ballMeasurement: BallMeasurement;
  public isValid: boolean = true;

  constructor(
    private dataService: DataService,
    private matDialog: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any
  ) {
  }

  createNewBall() {
    this.ballDetailForm.validate();
    if (this.ballMeasurement != null && this.isValid) {
      this.dataService.createBallMeasurement(this.dialogData.measurementId, this.ballMeasurement).then(() => {
        this.matDialog.close();
      }).catch(error => {
        alert('Nepodarilo sa uložiť loptu...');
      });
    }
  }
}
