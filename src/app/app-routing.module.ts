import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NewCategoryDialogComponent} from './new-category-dialog/new-category-dialog.component';
import {MeasurementDetailComponent} from './measurement-detail/measurement-detail.component';
import {PlaygroundComponent} from './playground/playground.component';
import {RealImpactPlaygroundComponent} from './real-impact-playground/real-impact-playground.component';
import {OrderDataTableComponent} from './order-data-table/order-data-table.component';
import {PlayersDetailsComponent} from './players-details/players-details.component';

const routes: Routes = [
  {path: '', component: LoginPageComponent},
  {path: 'merania', component: DashboardComponent},
  {path: 'dialog', component: NewCategoryDialogComponent},
  {path: 'meranie/:measurementId', component: MeasurementDetailComponent},
  {path: 'ihrisko/:measurementId', component: PlaygroundComponent},
  {path: 'dopad', component: RealImpactPlaygroundComponent},
  {path: 'tabulka', component: OrderDataTableComponent},
  {path: 'hrac', component: PlayersDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
