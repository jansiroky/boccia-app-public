import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculationService {

  constructor() {
  }

  average(array) {
    return array.reduce((sum, value) => sum + value, 0) / array.length;
  }

  standardDeviation(array) {
    const result = array.reduce((sum, value) => {
      return sum + Math.pow(parseFloat(value) - this.average(array), 2);
    }, 0);
    return Math.sqrt(result / array.length);
  }

  scattering(distancesArray) {
    if (distancesArray == null) {
      return 0;
    }
    return 2 * (this.average(distancesArray) + 2 * this.standardDeviation(distancesArray));
  }

  scatteringArea(A, B) {
    return A * B * 0.0001;
  }

  calculateEllipseXY(a, b, radius): {x: number; y: number} {
    const x = radius * Math.sqrt(a / b);
    const y = radius * Math.sqrt(a / b) / (a / b);
    return {x, y};
  }
}

