import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public uid: string;

  constructor(
    public afAuth: AngularFireAuth,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.uid = user.uid;
      }
    });
  }

  loginWithGoogle() {
    this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider()).then(value => {

    }).catch(error => {

    });
  }

  loginWithEmailLink(email) {
    const actionCodeSettings = {
      url: 'http://localhost:4200',
      handleCodeInApp: true
    };

    this.afAuth.auth.sendSignInLinkToEmail(email, actionCodeSettings).then(value => {

    }).catch(error => {

    });
  }

  logOut() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['']);
    });
  }
}
