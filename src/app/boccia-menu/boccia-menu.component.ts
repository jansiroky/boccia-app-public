import {Component, OnInit} from '@angular/core';
import {AuthService} from '../auth.service';
import {DataService} from '../data.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-boccia-menu',
  templateUrl: './boccia-menu.component.html',
  styleUrls: ['./boccia-menu.component.scss']
})
export class BocciaMenuComponent implements OnInit {

  public userName: string;
  public showMenu: boolean = true;

  constructor(
    public authService: AuthService,
    private dataService: DataService,
    private translateService: TranslateService,
  ) {
  }

  ngOnInit() {
    this.authService.afAuth.user.subscribe(user => {
      if (user == null) { // not logged in
        this.userName = '';
        this.showMenu = false;
      } else {
        this.userName = user.displayName ? user.displayName : user.email;
        this.showMenu = true;
        setTimeout(() => {
          this.dataService.getUserInfo().subscribe(userInfo => {
            if (userInfo != null && userInfo.name != null) {
              this.userName = userInfo.name;
            }
            if (userInfo != null && userInfo.language != null) {
              this.translateService.use(userInfo.language);
            }
          });
        }, 50);
      }
    });
  }
}
